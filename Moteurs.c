#include <msp430.h>
#define virage 300
#define vitesse 1700

void initMotors(){
    
    WDTCTL = WDTPW + WDTHOLD;//
    BCSCTL1= CALBC1_1MHZ; //frequence d’horloge 1MHz
    DCOCTL= CALDCO_1MHZ; // "
    
    //INIT MOTEUR 1
    P2DIR |= BIT2; // P2.1 en sortie
    P2SEL |= BIT2; // selection fonction TA1.1
    P2SEL2 &= ~BIT2; // selection fonction TA1.1
    
    //INIT MOTEUR 2
    P2DIR |= BIT4; // P2.5 en sortie
    P2SEL |= BIT4; // selection fonction TA1.2
    P2SEL2 &= ~BIT4; // selection fonction TA1.2    
    
    TA1CTL = TASSEL_2 | MC_1; // source SMCLK pour TimerA , mode comptage Up
    TA1CCTL1 |= OUTMOD_7; // activation mode de sortie n°7 sur TA1.1
    TA1CCTL2 |= OUTMOD_7; // activation mode de sortie n°7 sur TA1.2
    TA1CCR0 = 2000; // determine la periode du signal

}


//Fonction avancer 
void Avancer(){

    //avancer
    P2DIR |= (BIT1 | BIT5);
    P2SEL &= ~(BIT1 | BIT5);
    P2SEL2 &= ~(BIT1 | BIT5);
    //sens avant RG
    P2OUT &= ~BIT1;
    //sens avant RD
    P2OUT |= BIT5;

    //vitesse
    TA1CCR1 = vitesse;
    TA1CCR2 = vitesse;

}


void Ralentir(){

    //avancer
    P2DIR |= (BIT1 | BIT5);
    P2SEL &= ~(BIT1 | BIT5);
    P2SEL2 &= ~(BIT1 | BIT5);
    //sens avant RG
    P2OUT &= ~BIT1;
    //sens avant RD
    P2OUT |= BIT5;

    //vitesse
    TA1CCR1 = vitesse-400;
    TA1CCR2 = vitesse-400;

} 


//Fonction reculer 
void Reculer(){

    //avancer
    P2DIR |= (BIT1 | BIT5);
    P2SEL &= ~(BIT1 | BIT5);
    P2SEL2 &= ~(BIT1 | BIT5);
    //sens arrière RG
    P2OUT |= BIT1;
    //sens arrière RD
    P2OUT &= ~BIT5;

    //vitesse
    TA1CCR1 = vitesse;
    TA1CCR2 = vitesse;

}


//Fonctions Tourner
void TournerGauche(){
    
    //avancer
    P2DIR |= (BIT1 | BIT5);
    P2SEL &= ~(BIT1 | BIT5);
    P2SEL2 &= ~(BIT1 | BIT5);
    //sens avant RG
    P2OUT &= ~BIT1;
    //sens avant RD
    P2OUT |= BIT5;

    //vitesse
    TA1CCR1 = 0;
    TA1CCR2 = vitesse + virage;

}


void TournerDroite(){
    
    //avancer
    P2DIR |= (BIT1 | BIT5);
    P2SEL &= ~(BIT1 | BIT5);
    P2SEL2 &= ~(BIT1 | BIT5);
    //sens avant RG
    P2OUT &= ~BIT1;
    //sens avant RD
    P2OUT |= BIT5;

    //vitesse
    TA1CCR1 = vitesse + virage;
    TA1CCR2 = 0;

}


//Fonction demi-tour 
void DemiTour(){

    //Reculer Gauche 
    P2DIR |= BIT1;
    P2SEL &= ~BIT1;
    P2SEL2 &= ~BIT1;
    P2OUT |= BIT1;
    TA1CCR1 = 900;
    
    //Avancer Droit
    P2DIR |= BIT5;
    P2SEL &= ~BIT5;
    P2SEL2 &= ~BIT5;
    P2OUT |= BIT5;
    TA1CCR2 = 900;

    __delay_cycles(350000);
    //reinitialisation du sens des moteurs
    P2OUT &= ~BIT1;
    P2OUT |= BIT5;

}


void Stop(){

    //avancer
    P2DIR |= (BIT1 | BIT5);
    P2SEL &= ~(BIT1 | BIT5);
    P2SEL2 &= ~(BIT1 | BIT5);
    //sens avant RG
    P2OUT &= ~BIT1;
    //sens avant RD
    P2OUT |= BIT5;

    //vitesse
    TA1CCR1 = 0;
    TA1CCR2 = 0;   

}

        /* TEST
        ADC_Demarrer_conversion(0x01);
        mesure = ADC_Lire_resultat();
        luminosite = convert_Hex_Dec(mesure);
        Aff_valeur(luminosite);
    
        if(mesure > 500){
            Avancer();
        }else{
            Stop();
            __delay_cycles(1000000);
            DemiTour();
        }
        __delay_cycles(1500000);
        */

#define MOTORS_FREQUENCY 33333 // motors period T=30 ms (0.0333 s) soit 30 Hz
#define PW_MIN 648 //504 // 500/72=7 7*72=504
#define PW_MAX 2292 //2448 // 3000/72=41 41*72=2952

/*
* Headers
*/
#include <msp430g2231.h>

/*
* Prototypes
*/
void init_Board( void );
void init_Timer( void );

/* ----------------------------------------------------------------------------
* Fonction d'initialisation de la carte TI LauchPAD
* Entree : -
* Sorties: -
*/
void init_Board( void ){
    // Arret du "watchdog" pour eviter les redemarrages sur boucles infinies
    WDTCTL = WDTPW | WDTHOLD;

    // Calibration usine de l'oscillateur numerique interne
    if(CALBC1_1MHZ==0xFF || CALDCO_1MHZ==0xFF)
        __bis_SR_register(LPM4_bits);
    else
    {
        DCOCTL = 0;
        BCSCTL1 = CALBC1_1MHZ;
        DCOCTL = CALDCO_1MHZ;
    }

    //--- Securisation des entrees/sorties
    P1SEL = 0x00; // GPIO
    //P1SEL2 = 0x00; // GPIO
    P2SEL = 0x00; // GPIO
    //P2SEL2 = 0x00; // GPIO
    P1DIR = 0x00; // IN
    P2DIR = 0x00; // IN
    //---

    P1SEL |= BIT2; // Port 1, ligne 1 en fonction secondaire
    P1DIR |= BIT2; // Port 1, ligne 1 en sortie
}

/* ----------------------------------------------------------------------------
* FONCTION D'INITIALISATION DU TIMER
* Entree : -
* Sorties: -
*/
void init_Timer( void )
{
    TA0CTL &= ~CAP; // mode compare
    TA0CCR0 = MOTORS_FREQUENCY; // periode du signal PWM 50Hz
    TA0CTL = (TASSEL_2 | MC_1 | ID_0 | TACLR); // select TimerA source SMCLK, set mode to up-counting
    TA0CCTL1 = 0 | OUTMOD_7; // select timer compare mode
}

/* ----------------------------------------------------------------------------
* Fonction Principale
*/
void main(void)
{
    init_Board();
    init_Timer();

    TA0CCR1 = PW_MAX; //TA1CCR1 = cmd;

    while(1){
        if(TA0CCR1 == PW_MAX){
            TA0CCR1 = PW_MIN;

        }
        else if(TA0CCR1 == PW_MIN){
            TA0CCR1 = PW_MAX;
        }
        __delay_cycles(1000000);// delay 1s
    }
}

#include <msp430.h>

#define CMDLEN  10
unsigned char cmd[CMDLEN];

void initUART(void)
{
    P1SEL |= (BIT1 | BIT2);                     // P1.1 = RXD, P1.2=TXD
    P1SEL2 |= (BIT1 | BIT2);                    // P1.1 = RXD, P1.2=TXD
    UCA0CTL1 = UCSWRST;                         // SOFTWARE RESET
    UCA0CTL1 |= UCSSEL_3;                       // SMCLK (2 - 3)

    UCA0CTL0 &= ~(UCPEN | UCMSB | UCDORM);
    UCA0CTL0 &= ~(UC7BIT | UCSPB | UCMODE_3 | UCSYNC); // dta:8 stop:1 usci_mode3uartmode
    UCA0CTL1 &= ~UCSWRST;                   // **Initialize USCI state machine**

    UCA0BR0 = 104;                              // 1MHz, OSC16, 9600 (8Mhz : 52) : 8/115k
    UCA0BR1 = 0;                                // 1MHz, OSC16, 9600
    UCA0MCTL = 10;

    /* Enable USCI_A0 RX interrupt */
    IE2 |= UCA0RXIE;
}

void TXdata( unsigned char c)
{
    while (!(IFG2 & UCA0TXIFG));  // USCI_A0 TX buffer ready?
    UCA0TXBUF = c;              // TX -> RXed character
}

void RXdata(unsigned char *c)
{
    while (!(IFG2&UCA0RXIFG));              // buffer Rx USCI_A0 plein ?
    *c = UCA0RXBUF;
}

void Send_STR_UART(const char *msg)
{
    int i = 0;
    for(i=0 ; msg[i] != 0x00 ; i++)
    {
        TXdata(msg[i]);
    }
}

void interpreteur( void )
{
    __bis_SR_register(GIE); // interrupts enabled
    unsigned char c;

        c = UCA0RXBUF;
        TXdata(c);
    if(c == 'o')          //----------------------------------- help
    {
        Send_STR_UART("\r\nCommandes :");
        Send_STR_UART("\r\n'e' : avancer");
        Send_STR_UART("\r\n'd' : reculer");
        Send_STR_UART("\r\n's' : tounerGauche");
        Send_STR_UART("\r\n'f' : tounerDroit");
        Send_STR_UART("\r\n'k' : Stop");
        Send_STR_UART("\r\n'p' : Arrete urgent");
        Send_STR_UART("\r\n'k' : Stop");
        Send_STR_UART("\r\n'1' : Go");


        Send_STR_UART("\r\n'h' : help\r\n");
    }
    else if (c == '1')
    {
        Send_STR_UART("\r\n");
        Send_STR_UART(c);
        Send_STR_UART("->");
        //Send_char_SPI(0x30); // Send '0' over SPI to Slave
        Send_STR_UART("\r\n");
    }
    else if (c == '0')
    {
        Send_STR_UART("\r\n");
        Send_STR_UART(c);
        Send_STR_UART("->");
        //Send_char_SPI(0x31); // Send '1' over SPI to Slave
        Send_STR_UART("\r\n");
    }

    else                          //---------------------------- default choice
    {
        Send_STR_UART("\r\n ?");
        Send_STR_UART(c);

    }
}
int IR(void){

    P1DIR |= BIT6;

    //Vérification d'obstacle

    if(P1IN & BIT1){

        P1OUT |= BIT6;

       return 1;

    }
    else{

    P1OUT &=~ BIT6;

    return 0;
    }

}


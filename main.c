#include <msp430g2553.h>
#include <Moteurs.h>
#include <stdlib.h>
#include<stdio.h>
#include <UART_2553.h>
#include <string.h>

#define RELEASE "\r\t\tSPI-rIII162018"
#define PROMPT  "\r\nmaster>"
#define CMDLEN  10

#define TRUE    1
#define FALSE   0

#define LF      0x0A            // line feed or \n
#define CR      0x0D            // carriage return or \r
#define BSPC    0x08            // back space
#define DEL     0x7F            // SUPRESS
#define ESC     0x1B            // escape

#define _CS         BIT4            // chip select for SPI Master->Slave ONLY on 4 wires Mode
#define SCK         BIT5            // Serial Clock
#define DATA_OUT    BIT6            // DATA out
#define DATA_IN     BIT7            // DATA in

unsigned char cmd[CMDLEN];      // tableau de caracteres lie a la commande user
unsigned char car = 0x30;       // 0
unsigned int  nb_car = 0;
int obstacle;


unsigned char intcmd = FALSE;   // call interpreteur()


void main(void){
    // Stop watchdog timer to prevent time out reset
    WDTCTL = WDTPW | WDTHOLD;

    if(CALBC1_1MHZ==0xFF || CALDCO_1MHZ==0xFF)
    {
        __bis_SR_register(LPM4_bits); // Low Power Mode #trap on Error
    }
    else
    {
        // Factory parameters
        BCSCTL1 = CALBC1_1MHZ;
        DCOCTL = CALDCO_1MHZ;
    }

    initUART();
    initMotors();
    P1DIR |= BIT1;


    Send_STR_UART("\rReady !\r\n");

    while(1)
    {
        obstacle = IR();
        if (obstacle)
        {
            Stop();
        }
           if( intcmd )
           {
                   char c = UCA0RXBUF;
                   TXdata(c);

                   if (c=='m'){
                      intcmd = FALSE;
                      //TA0CCTL1 = CCIFG;
                      __bis_SR_register(LPM4_bits | GIE); // interrupts enabled
                   }
                   else{
                       __disable_interrupt();
                   }
           }
           else
           {
               __bis_SR_register(LPM4_bits | GIE); // general interrupts enable & Low Power Mode
           }
       }




    }


// Echo back RXed character, confirm TX buffer is ready first
#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
    unsigned char c;

    c = UCA0RXBUF;
    TXdata(c);
    TA0CCTL1 = ~CCIFG;

   if (c=='e'){
               Avancer();
      }
   if (c=='d'){
          Reculer();
      }
   if (c=='s'){
          TournerGauche();
      }
   if (c=='f'){
          TournerDroite();
      }
   if (c=='k'){
          Stop();
      }
   if (c=='o'|| c=='1'||c=='0'){
                  Stop();
                  interpreteur();

              }

   if(c=='p'){
        Stop();
        intcmd = TRUE;
        __bic_SR_register_on_exit(LPM4_bits);   // OP mode !

    }



}

#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
{
    if(P1IFG&BIT1)  //is there interrupt pending?
        {
          if(!(P1IES&BIT1)) // is this the rising edge?
          {
            Stop();
          }

    P1IFG &= ~BIT1;             //clear flag
    }
}
